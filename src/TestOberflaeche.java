import java.awt.*;
import javax.swing.*;

public class TestOberflaeche{

	/*
	#ToDo-List: 
	#	Shortcuts for to allow fast input
	#	Adding Buttons: Delete, Sinus, Cosinus, Tangens, ANS, Memory ( S1-S5 )
	#	Implement Functions: Delete, Memory
	*/
	 

	public static void addComponentsToPane(Container pane) {
		
		
		//Schrift von links nach rechts ... kann ignoriert werden
		pane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT); 
		
		//Layout festlegen										
		pane.setLayout(new GridBagLayout());	
		
		//DazugehÑ†rige Constraints initialisieren, werden spÐ´ter bei jedem Button deklariert
		GridBagConstraints c = new GridBagConstraints();
		
		final JTextArea test = new JTextArea(" ");
		test.setFont(new Font("Serif", Font.PLAIN, 20));
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		c.gridwidth = 5;	//Zahl der einzunehmenden Zellen
		c.ipady = 20;		//HÑ†he manuell festlegen
		c.ipadx = 250;
		c.gridx = 0;
		c.gridy = 0;
		pane.add(test,c);
		
		//Numbers
		JButton button1 = new JButton("1");
		c.ipady = 0;
		c.ipadx = 0;
		c.gridwidth = 1;
		c.weightx = 0.5;// Freier Platz im Fenster wird gleichmÐ´Ñ�ig verteilt
		c.fill = GridBagConstraints.HORIZONTAL;	//Elemente werden Horizontal an die FenstergrÑ†Ñ�e angepasst
		c.gridx = 0;
		c.gridy = 1;		
		button1.addActionListener(new java.awt.event.ActionListener(){
			// Beim DrÑŒcken des MenÑŒpunktes wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Programm schlieÑ�en
            	
                test.setText(test.getText() + "1" );
            }
		});
		pane.add(button1,c);
		
		
		
		JButton button2 = new JButton("2");
		c.weightx = 0.5;// Freier Platz im Fenster wird gleichmÐ´Ñ�ig verteilt
		c.fill = GridBagConstraints.HORIZONTAL;	//Elemente werden Horizontal an die FenstergrÑ†Ñ�e angepasst
		c.gridx = 1;
		c.gridy = 1;
		button2.addActionListener(new java.awt.event.ActionListener(){
			// Beim DrÑŒcken des MenÑŒpunktes wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Programm schlieÑ�en
            	
                test.setText(test.getText() + "2" );
            }
		});
		pane.add(button2,c);
		
		
		
		JButton button3 = new JButton("3");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 2;
		c.gridy = 1;
		button3.addActionListener(new java.awt.event.ActionListener(){
			// Beim DrÑŒcken des MenÑŒpunktes wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Programm schlieÑ�en
            	
                test.setText(test.getText() + "3" );
            }
		});
		pane.add(button3,c);
		
		
		
		JButton button4 = new JButton("4");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 0;
		c.gridy = 2;
		button4.addActionListener(new java.awt.event.ActionListener(){
			// Beim DrÑŒcken des MenÑŒpunktes wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Programm schlieÑ�en
            	
                test.setText(test.getText() + "4" );
            }
		});
		pane.add(button4,c);
		
		
		
		JButton button5 = new JButton("5");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 1;
		c.gridy = 2;		
		button5.addActionListener(new java.awt.event.ActionListener(){
			// Beim DrÑŒcken des MenÑŒpunktes wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Programm schlieÑ�en
            	
                test.setText(test.getText() + "5" );
            }
		});
		pane.add(button5,c);
		
		
		
		JButton button6 = new JButton("6");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 2;
		c.gridy = 2;
		button6.addActionListener(new java.awt.event.ActionListener(){
			// Beim DrÑŒcken des MenÑŒpunktes wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Programm schlieÑ�en
            	
                test.setText(test.getText() + "6" );
            }
		});
		pane.add(button6,c);
		
		
		
		JButton button7 = new JButton("7");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 0;
		c.gridy = 3;
		button7.addActionListener(new java.awt.event.ActionListener(){
			// Beim DrÑŒcken des MenÑŒpunktes wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Programm schlieÑ�en
            	
                test.setText(test.getText() + "7" );
            }
		});
		pane.add(button7,c);
		
		
		
		JButton button8 = new JButton("8");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 1;
		c.gridy = 3;
		button8.addActionListener(new java.awt.event.ActionListener(){
			// Beim DrÑŒcken des MenÑŒpunktes wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Programm schlieÑ�en
            	
                test.setText(test.getText() + "8" );
            }
		});
		pane.add(button8,c);
		
		
		
		JButton button9 = new JButton("9");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 2;
		c.gridy = 3;
		button9.addActionListener(new java.awt.event.ActionListener(){
			// Beim DrÑŒcken des MenÑŒpunktes wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Programm schlieÑ�en
            	
                test.setText(test.getText() + "9" );
            }
		});
		pane.add(button9,c);
		
	
		
		JButton button0 = new JButton("0");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridwidth = 2;
		c.gridx = 0;
		c.gridy = 4;
		button0.addActionListener(new java.awt.event.ActionListener(){
			// Beim DrÑŒcken des MenÑŒpunktes wird actionPerformed aufgerufen
            public void actionPerformed(java.awt.event.ActionEvent e) {
                //Programm schlieÑ�en
            	
                test.setText(test.getText() + "0" );
            }
		});
		pane.add(button0,c);
		
		
		//Zeichen und Rechenoperationen
		JButton buttonPoint = new JButton(",");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridwidth = 1;
		c.gridx = 2;
		c.gridy = 4;
		buttonPoint.addActionListener(new java.awt.event.ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent e) {
                test.setText(test.getText() + "." );
            }
		});
		pane.add(buttonPoint,c);
		
		
		
		//Functions
		JButton buttonPlus = new JButton("+");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 3;
		c.gridy = 1;	
		buttonPlus.addActionListener(new java.awt.event.ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent e) {
                test.setText(test.getText() + " + " );
            }
		});
		pane.add(buttonPlus,c);
		
		
		
		JButton buttonMinus = new JButton("-");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 3;
		c.gridy = 2;
		buttonMinus.addActionListener(new java.awt.event.ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent e) {
                test.setText(test.getText() + " - " );
            }
		});
		pane.add(buttonMinus,c);
		
		
		
		JButton buttonMult = new JButton("*");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 4;
		c.gridy = 1;
		buttonMult.addActionListener(new java.awt.event.ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent e) {
                test.setText(test.getText() + " * " );
            }
		});
		pane.add(buttonMult,c);
		
		
		
		JButton buttonDiv = new JButton("/");
		c.weightx = 0.5;
		c.fill = GridBagConstraints.HORIZONTAL;	
		c.gridx = 4;
		c.gridy = 2;
		buttonDiv.addActionListener(new java.awt.event.ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent e) {
                test.setText(test.getText() + " / " );
            }
		});
		pane.add(buttonDiv,c);
		
		
		
		JButton buttonOpen = new JButton("(");
		c.gridx = 3;
		c.gridy = 3;
		buttonOpen.addActionListener(new java.awt.event.ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent e) {
                test.setText(test.getText() + " ( " );
            }
		});
		pane.add(buttonOpen,c);
		
		
		
		JButton buttonClose = new JButton(")");
		c.gridx = 4;
		c.gridy = 3;
		buttonClose.addActionListener(new java.awt.event.ActionListener(){
            public void actionPerformed(java.awt.event.ActionEvent e) {
            	//Falls davor ein Leerzeichen ist --> keine Klammer zu
            	if( !Character.isWhitespace(test.getText().charAt(test.getText().length()-1 ) ) )
            		test.setText(test.getText() + " ) " );
            }
		});
		pane.add(buttonClose,c);
		
		
		
		JButton buttonResult = new JButton("=");
		c.weightx = 0.5;
		c.gridx = 3;
		c.gridy = 4;
		c.gridwidth = 2;
		//buttonPlus.addActionListener(new java.awt.event.ActionListener(){
        //	public void actionPerformed(java.awt.event.ActionEvent e) {
        //	}
		//});
		pane.add(buttonResult,c);
		
	}
	 /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
	
	
	public static void showGUI(){
		//Create and set up the window.
		JFrame frame = new JFrame("Taschenrechner");
		frame.setSize(400, 300);
		frame.setLocation(200, 200);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		 //Set up the content pane.
        addComponentsToPane(frame.getContentPane());
        
      //Display the window.
        frame.pack();			//Sorgt fÑŒr mÑ†glichst wenig ungenutzten Platz im Fenster
        frame.setVisible(true);
		
	}
	
	
	
	public static void main(String[] args) {
	//Schedule a job for the event-dispatching thread:
	//creating and showing this application's GUI.
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				showGUI();
			}
		});
	}
	
		
}

